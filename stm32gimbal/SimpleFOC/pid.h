#ifndef PID_H
#define PID_H

/******************************************************************************/
void PID_init0(void);
float PID_velocity0(float error);
float PID_angle0(float error);
void PID_init(void);
float PID_velocity(float error);
float PID_angle(float error);
/******************************************************************************/

#endif

